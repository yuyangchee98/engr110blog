---
layout: page
title: About
permalink: /about/
---

I am Yu Yang Chee, a Computer Science major in Santa Clara Univerity (Class of 2022). Unsurprisingly, I like computers, it lets me be creative as I can always make things with it, and with the internet I can share it to whoever I want almost instantly. Furthermore, mistakes are more tolerable! If I screw something up, CRTL+Z is always there to save me. 

I also love jazz. My favorite thing to brag about is that I have a jazz playlist with 24 followers on Spotify. Check it out 
[here](https://open.spotify.com/user/yuyangchee98/playlist/3dii4QmsgOe0iEf65W1vtf?si=LWit0DYSQy6R1O3OJQrd1A)!

The team members working on the stadiometer are: Luis, Angel, Dominic and Yu Yang. We are a team of mechanical engineer, bioengineer, computer engieer and computer scientist. 

Group members' eFolio's:
- Dominic: https://magdaluyo.weebly.com
- Angel: https://acardenas0.wixsite.com/angelcardenas