---
layout: post
title:  "Week 4- Testing the IR sensor"
date:   2019-02-02 12:52:00 -0800
categories: Summary
---

We purchased an infrared sensor last week and it arrived on Tuesday. We then scheduled a group meeting on Thursday to try to hook the sensor up with the Arduino and write some code so that we can begin some testing on Saturday.

After reflecting from the object, the beam will be directed through the second lens on a position-sensible photo detector (PSD). The conductivity of this PSD depends on the position where the beam falls. The conductivity is converted to voltage and if the voltage is digitalized by using analogue-digital converter, the distance can be calculated. 

The sensor will shoot a beam toward the floor, which will then be reflected back thorugh the second lens on a position-sensible photo detector (PSD). The conductivity of the PSD depends on the position where the beam falls. The conductivity is converted into analog output voltage, and we can convert it into distance by using an analogue-digital converted and then converting it into distance.

![UtoD](https://home.roboticlab.eu/_media/examples/sensor/ir_distance/sensor_ir_distance_graph.png?w=240&tok=fb93ee)
[Source](https://home.roboticlab.eu/en/examples/sensor/ir_distance)

Our approach of using IR sensors as opposed to the previous team's use of ultrasound should also increase the speed of the readings as we are dealing with speed of light as opposed to speed of sound. This would be an unplanned benefit.

On Thursday we managed to get the sensors hooked up to the Arduino and wrote code to print voltage value

Our Saturday meeting went well, we managed to collect some data to verify the relationship between voltage and distance. I was noting down the values while Luis was adjusting the distance of the IR sensor. And with that, we could convert the voltage values into distance.

![Image](https://i.imgur.com/vIPRtDU.jpg)

At this point the values are still not very accurate as we need to collect more accurate data in order to get better constants for our equation that relates voltage and distance. 

We have also drafted up a couple of designs and came to the conclusion that a rectangular shaped design would best fit our purposes. This housing for the stadiometer would be similar to the first groups design.

![Image](https://i.imgur.com/QHypXT5.jpg)


![Image](https://i.imgur.com/FcYAd9d.jpg)

Our next steps for the near future are:
- Plan and execute an experiment to get more accurate results of voltage and distance. This can help us figure out a more accurate constant.
- Decide on the dimensions of the device
- Figure out how to use the gyroscope
- Thinking about human error in 3D in addition to only 2D.

