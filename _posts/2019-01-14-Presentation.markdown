---
layout: post
title:  "Presentation of this blog"
date:   2019-01-12 14:32:35 -0800
categories: Technical-information
---

Current state summary: 
As we're entering the second week of the quarter, we will be deciding on what projects to work on. I am most interested in measuring food waste in SCU. 

All updates will be posted as a post in this blog and categorization filter so you can quickly filter out what you want to (or do not want to) read. The categories include: 
- summaries of project status
- research, thoughts, choices made
- team meeting documentation
- customer interaction notes
- formal project information 