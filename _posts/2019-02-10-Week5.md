---
layout: post
title:  "Week 5- Finding the constant"
date:   2019-02-10 09:52:00 -0800
categories: Summary
---

Each IR sensor is built a little differently, hence there isn't one single equation that can convert voltage to distance accurately. This week, we set out to find the right constant to build our equation. We met up in the library on Saturday, and took a bunch of books to stack them with increments of 2 inches to find record the data.

![Imgur](https://i.imgur.com/OfoaEn6.jpg)

We ended up with a curve like this, where x-axis is distance in inches and y-axis is the voltage value.

![Imgur](https://i.imgur.com/QT50TvX.png)

I set up the Google Sheets to note down our data as well as created pages/ columns for equations (We looked at mean, standard deviation, mode, variance, standard deviation of the mean) and graphs. We were also uisng my laptop to upload the code to arduino as there was a bug on Luis' Arduino IDE that didn't allow him to copy the data.

We all have tasks to do before we meet on again on Tuesday. Luis will be taking a look at how to convert the data into the right equation. I will be writing code for the production product to display averaged values. Angel will be looking at sending a hand-made prototype to Nikki from Pediatric Wellness Center and Dom will be looking at how to write code for the gyroscope.s

We are increasing the length of the device to decrease the chances that the IR sensor interference. 

Our next steps are to collect data for the second IR sensor, CAD the design after Angel comes back from Nikki, and print the device by this weekend.