---
layout: post
title:  "Week 7- Fixing the details"
date:   2019-02-25 10:23:00 -0800
categories: Summary
---

As Nikki from Pediatric Wellness Group was out of town last week and missed our update presentation, we couldn't get her feedback on our current progress like the design. We didn't want to print our stadiometer before getting feedback on the product, so we are using a cardboard box temporarily to hold the components.

![Imgur](https://i.imgur.com/o592K0g.jpg)

![Imgur](https://i.imgur.com/8PqMjhH.png)

I also went to the Makerlab tuesday morning to solder the gyroscope as the compoenent was very loose without solder and would frequently lose connection.

I couldn't attend the group meeting over the weekend as I was at MAA Sectional Meeting, a mathematics event. However, I did communicate with my group members after the event to get updated on the progress of the project. 

One of the problems that we are facing is that the sensor seems to act weirdly when the height is under 100cm, we are currently still investigating the issue. We also implemented an algorithm to reject values that are outside of the acceptable range using statistical methods.

I am currently working on writing the design review reflection, and it should be done soon.