---
layout: page
title: Community Partner
permalink: /community-partner/
---

![Pediatric Wellness Group](https://pediatricwellnessgroup.com/wp-content/themes/pediatricwellnessgroup.com-theme/images/site-design/logo.png)

The community parter that my group will be working with is [Pediatric Wellness Group](https://pediatricwellnessgroup.com). The mission of their organization is to blend modern medicine with the core values of compassion, empathy & respect. They seek to partner with parents to create positive change through the integration of physical, emotion, and intellectual health, one child at a time. They have 5 physicians on site and offers a range of services such as personalized care integrated care same day, evening and weekend appointments for urgent problems, continuity of care and a medical home. A complete stadiometer could make nurses’ lives easier, help doctors make better judgement and give parents better data on their child. The amount of impact depends on the amount of patients in Pediatric Wellness Group. Some obvious needs for this product include measuring a child’s height within seconds and that the device must be small enough such that it can be transported from room to room. They are located in Redwood City.